package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
@SpringBootApplication
public class SpringbootappApplication {
	
	@RequestMapping("/")
    String home() {
		System.out.println("Check");
        return "Hello World!! Openshift now";
    }

	public static void main(String[] args) {
		SpringApplication.run(SpringbootappApplication.class, args);
	}
}
